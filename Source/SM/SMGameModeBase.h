// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SMGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SM_API ASMGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
