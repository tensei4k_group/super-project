// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SM_SaveGame.generated.h"

/**
 * 
 */
UCLASS()
class SM_API USM_SaveGame : public USaveGame
{
	GENERATED_BODY()
	
};
