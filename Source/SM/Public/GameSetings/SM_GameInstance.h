// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SM_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SM_API USM_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
