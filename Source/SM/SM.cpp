// Copyright Epic Games, Inc. All Rights Reserved.

#include "SM.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SM, "SM" );
